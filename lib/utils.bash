#!/usr/bin/env bash

set -euo pipefail

APPIMAGE_REPO="https://appimages.libreitalia.org/"
TOOL_NAME="calc"
TOOL_TEST="calc"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_all_versions() {
  curl -s $APPIMAGE_REPO | grep basic | grep -o -E "LibreOffice-[0-9]+.*\.AppImage" | grep -E "LibreOffice-[0-9](\.*[0-9]+)*" | cut -d"-" -f2 | cut -d"b" -f1 | sed 's/\.$//' | uniq
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  url="$APPIMAGE_REPO/LibreOffice-${version}.basic-x86_64.AppImage"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi
  
  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/calc"
    echo "#!/bin/bash" >> "$install_path/bin/calc"
    echo "$install_path/calc.AppImage --calc \"\$@\"" >> "$install_path/bin/calc"
    chmod a+x "$install_path/bin/calc"
    chmod a+x "$install_path/calc.AppImage"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
